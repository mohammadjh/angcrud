(function() {
    'use strict';

    angular
        .module('angcrud.directives')
        .directive('crudTable', function($parse){
            return {
                restrict: 'E',
                scope: {
                    cols: '=',
                    api: '=',
                },
                link: function(scope, element, attrs) {
                    //scope.cols = $parse(attrs.cols);
                    console.log(scope.api);


                   /* {
                        name: 'field name',
                        label: 'label name',
                        type: 'field type'
                    }*/
                },
                templateUrl: 'angcrud.html'
            }
        });
})();