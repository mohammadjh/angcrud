(function() {
    'use strict';

    angular
        .module('angcrud', [
            'angcrud.directives',
            'product.services',
            'product.controllers',
            'ui.bootstrap'
        ]);

    angular
        .module('angcrud.directives', []);

    angular
        .module('product.services', []);

    angular
        .module('product.controllers', []);
})();