(function() {
    'use strict';

    angular
        .module('product.controllers')
        .controller('ProductController', ProductController)

    ProductController.$inject = ['$scope', 'Product'];

    function ProductController($scope, Product) {
        /*$scope.all = all;

        function all() {
            Product.all().then(successFn, errorFn);

            function successFn(data) {
                $scope.products = data;
                console.log(data);
            }

            function errorFn(error) {
                console.log('Error');
            }
        }*/

        Product.all(function(data) {
            $scope.products = data.data;
        })
    }
})();